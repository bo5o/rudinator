from setuptools import find_packages, setup

setup(
    name="rudinator",
    version="0.1.0",
    description="Next level baby simulation",
    author="Schmiddi & Bossi",
    license="MIT",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    package_data={
        "rudinator": ["py.typed"],
    },
    python_requires="==3.7.10",
    zip_safe=False,
    install_requires=[
        "gpiozero",
    ],
    extras_require={
        "rpi": [
            "rpi.gpio",
            "spidev",
        ]
    },
)
