import string
import time

import pytest
from freezegun import freeze_time
from gpiozero import Button, LEDCharDisplay
from pytest_mock import MockFixture

from rudinator.devices import Joker


@pytest.mark.usefixtures("mock_pins")
class TestJoker:
    @freeze_time("2021-06-18 18:00:00")
    def test_init(self):
        joker = Joker()
        assert isinstance(joker.display, LEDCharDisplay)
        assert isinstance(joker.button, Button)

        assert isinstance(joker.choices, str)
        assert len(joker.choices) > 1
        assert joker.display.value == " "
        assert joker.count == 3
        assert joker.refill_time == 1800
        assert joker.next_refill > time.time()

    def test_show(self):
        joker = Joker()
        joker.show()
        assert joker.display.value in string.ascii_uppercase

    def test_redeem(self, mocker: MockFixture):
        joker = Joker()
        show_spy = mocker.spy(joker, "show")
        start_count = joker.count
        joker.redeem()
        assert joker.count == start_count - 1
        show_spy.assert_called_once()

    def test_redeem_all(self, mocker: MockFixture):
        joker = Joker()
        show_spy = mocker.spy(joker, "show")
        start_count = joker.count
        while joker.count > 0:
            joker.redeem()
        joker.redeem()
        assert joker.count == 0
        assert show_spy.call_count == start_count

    def test_update_full(self):
        joker = Joker()
        start_count = joker.count
        joker.update()
        assert joker.count == start_count

    def test_update_refill(self):
        with freeze_time("2021-06-13 23:30:00") as frozen_time:
            joker = Joker()
            start_count = joker.count
            joker.redeem()
            assert joker.count < start_count
            frozen_time.tick(joker.refill_time)
            joker.update()
            assert joker.count == start_count
            assert joker.next_refill > time.time()
