import pytest
from gpiozero import Device
from gpiozero.pins.mock import MockFactory


@pytest.fixture(scope="function")
def mock_pins(request):
    save_factory = Device.pin_factory
    Device.pin_factory = MockFactory()
    try:
        yield Device.pin_factory
    finally:
        if Device.pin_factory is not None:
            Device.pin_factory.reset()
        Device.pin_factory = save_factory
