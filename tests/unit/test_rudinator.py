import time
from unittest.mock import Mock

import pytest
from freezegun import freeze_time
from gpiozero.pins.mock import MockFactory as MockPinFactory
from pytest_mock import MockFixture

from rudinator import Condition, Rudi
from rudinator.devices import BabyCry, Joker, LEDStrip, Reservoir, SmoothSensor


@pytest.fixture(name="rudi", scope="function")
def fixture_rudi():
    return Rudi()


@pytest.fixture(scope="function", autouse=True)
def _use_mock_pins(mock_pins: MockPinFactory):
    """Use mock pin factory for all tests in this module."""


@freeze_time("2021-06-12 18:30:00")
def test_initialize():
    rudi = Rudi()
    assert isinstance(rudi.mouth, SmoothSensor)
    assert isinstance(rudi.belly, SmoothSensor)
    assert isinstance(rudi.vestibular, SmoothSensor)

    assert isinstance(rudi.voice, BabyCry)
    assert isinstance(rudi.blatter, Reservoir)

    assert isinstance(rudi.joker, Joker)
    assert isinstance(rudi.light, LEDStrip)

    assert rudi.last_pee == 0
    assert rudi.next_change - time.time() < 100

    assert isinstance(rudi.condition, Condition)
    assert not any(rudi.condition)


def test_change_condition():
    rudi = Rudi()
    rudi.change_condition("sick")
    assert rudi.condition.sick
    assert not rudi.condition.sleepy
    assert not rudi.condition.thirsty


def test_change_condition_fail():
    rudi = Rudi()
    with pytest.raises(TypeError):
        rudi.change_condition("happy")  # Rudi is never happy


def test_reset_condition():
    rudi = Rudi()
    rudi.change_condition("sick")
    assert rudi.condition.sick
    rudi.reset_condition("sick")
    assert not rudi.condition.sick


def test_update_under_condition(mocker: MockFixture):
    rudi = Rudi()
    rudi.change_condition("sick")
    pee = mocker.patch.object(rudi, "pee")
    cry = mocker.patch.object(rudi, "cry")

    rudi.update()
    pee.assert_called_once()
    cry.assert_called_once()


@freeze_time("2021-06-12 23:54:00")
def test_update_no_condition():
    rudi = Rudi()
    rudi.update()
    assert not any(rudi.condition)
    assert rudi.next_change > time.time()


def test_update_condition_change(mocker: MockFixture):
    with freeze_time("2021-06-12 23:54:00") as frozen_time:
        rudi = Rudi()
        mocker.patch.object(rudi, "pee")
        mocker.patch.object(rudi, "cry")
        rudi.update()
        assert not any(rudi.condition)
        frozen_time.move_to("2021-06-12 23:59:00")
        rudi.update()
        condition = rudi.condition
        assert any(condition)
        assert rudi.next_change - time.time() >= 900
        frozen_time.move_to("2021-06-13 00:04:00")
        rudi.update()
        assert rudi.condition == condition


@freeze_time("2021-06-12 23:54:00")
def test_pee():
    mock_blatter = Mock(spec=Reservoir)
    rudi = Rudi(blatter=mock_blatter)
    assert rudi.last_pee == 0
    rudi.change_condition("thirsty")
    rudi.pee()
    assert rudi.last_pee == time.time()
    mock_blatter.release_liquid.assert_called_once_with("yellow", 3)


@freeze_time("2021-06-12 23:54:00")
def test_pee_break():
    mock_blatter = Mock(spec=Reservoir)
    rudi = Rudi(blatter=mock_blatter)
    rudi.change_condition("sick")
    rudi.pee()
    rudi.change_condition("thirsty")
    rudi.pee()
    mock_blatter.release_liquid.assert_called_once_with("green", 3)


def test_cry():
    mock_voice = Mock(spec=BabyCry)
    rudi = Rudi(voice=mock_voice)
    rudi.cry()
    mock_voice.play.assert_called_once()


def test_feed():
    rudi = Rudi()
    rudi.change_condition("thirsty")
    rudi.mouth.wait_for_inactive(1)
    rudi.mouth.pin.drive_high()
    rudi.mouth.wait_for_active(1)
    assert not rudi.condition.thirsty
    assert not any(rudi.condition)


def test_feed_wrong():
    rudi = Rudi()
    rudi.change_condition("sick")
    rudi.mouth.wait_for_inactive(1)
    rudi.mouth.pin.drive_high()
    rudi.mouth.wait_for_active(1)
    assert rudi.condition.sick


def test_belly_massage():
    rudi = Rudi()
    rudi.change_condition("sick")
    rudi.belly.wait_for_inactive(1)
    rudi.belly.pin.drive_high()
    rudi.belly.wait_for_active(1)
    assert not rudi.condition.sick


def test_lull_to_sleep():
    rudi = Rudi()
    rudi.change_condition("sleepy")
    rudi.vestibular.wait_for_inactive(1)
    rudi.vestibular.pin.drive_high()
    rudi.vestibular.wait_for_active(1)
    assert not rudi.condition.sleepy
