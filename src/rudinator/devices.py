import random
import string
import time
import typing

from gpiozero import (
    Button,
    DigitalOutputDevice,
    LEDCharDisplay,
    SmoothedInputDevice,
    SPIDevice,
)

__all__ = ["SmoothSensor", "LEDStrip", "Joker", "Reservoir", "BabyCry"]


class SmoothSensor(SmoothedInputDevice):
    """Generic sensor that takes average over historic active states."""

    def __init__(
        self,
        pin=None,
        pull_up=False,
        active_state=None,
        queue_len=10,
        sample_wait=0.1,
        threshold=0.5,
        partial=False,
        pin_factory=None,
    ) -> None:
        super().__init__(
            pin,
            pull_up=pull_up,
            active_state=active_state,
            threshold=threshold,
            queue_len=queue_len,
            sample_wait=sample_wait,
            partial=partial,
            pin_factory=pin_factory,
        )
        self._queue.start()


class LEDStrip(SPIDevice):
    """LED strip to create some light effects."""

    # TODO: configure SPI device
    # TODO: implement some light effects


class Joker:
    """A char display and a button to redeem jokers."""

    def __init__(
        self,
        display_pins: typing.Optional[typing.Iterable[int]] = None,
        button_pin: typing.Optional[int] = 12,
    ) -> None:
        display_pins = display_pins or (5, 6, 13, 19, 26, 16, 20)
        self.display = LEDCharDisplay(*display_pins)
        self._choices = "".join(
            letter for letter in self.display.font if letter in string.ascii_uppercase
        )
        self.button = Button(button_pin, pull_up=True)
        self.button.when_pressed = self.redeem
        self._count = 3
        self.refill_time = 1800
        self._next_refill = time.time() + self.refill_time

    @property
    def count(self) -> int:
        return self._count

    @property
    def next_refill(self) -> float:
        return self._next_refill

    @property
    def choices(self) -> str:
        return self._choices

    def update(self) -> None:
        # add a joker every once in a while
        current_time = time.time()
        if current_time >= self._next_refill:
            self._count = min(self._count + 1, 3)
            self._next_refill = current_time + self.refill_time

    def redeem(self) -> None:
        if self._count > 0:
            self.show()
            self._count -= 1

    def show(self) -> None:
        self.display.value = random.choice(self._choices)


class Pump(DigitalOutputDevice):
    """A pump to release liquid from a reservoir."""

    def pump(self, duration: float):
        self.blink(on_time=duration, n=1, background=True)


class Reservoir:
    """A reservoir to contain multiple liquids in separated chambers."""

    def __init__(self, **chambers: int):
        self._pumps = {chamber: Pump(pin) for chamber, pin in chambers.items()}

    def release_liquid(self, chamber: str, duration: float):
        self._pumps[chamber].pump(duration=duration)


class BabyCry:
    """An audio device that can play the sound of a baby crying."""

    def play(self) -> None:
        # TODO: implement audio playback
        pass
