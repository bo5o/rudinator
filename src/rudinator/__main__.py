from signal import pause

from rudinator import Rudi
from rudinator.periodic import periodic_task

rudi = Rudi()


@periodic_task(5)
def main():
    rudi.update()


stop = main()

pause()
