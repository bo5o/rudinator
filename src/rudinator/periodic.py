"""Helpers to create periodic tasks.

Taken from [this gist](https://gist.github.com/Depado/7925679), adjusted to account for
time drift.
"""
import threading
import time


def periodic_task(interval: float, times: int = -1):
    """Decorator to run a function periodically in a background thread."""

    def outer_wrap(function):
        def wrap(*args, **kwargs):
            stop = threading.Event()

            def inner_wrap():
                i = 0
                next_interval = interval
                while i != times and not stop.is_set():
                    next_call = time.time() + next_interval
                    stop.wait(next_interval)
                    function(*args, **kwargs)
                    i += 1
                    next_interval = max(interval - (time.time() - next_call), 0)

            t = threading.Timer(0, inner_wrap)
            t.daemon = True
            t.start()
            return stop

        return wrap

    return outer_wrap
