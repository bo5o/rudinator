import random
import time
import typing

from rudinator.devices import BabyCry, Joker, LEDStrip, Reservoir, SmoothSensor


class Condition(typing.NamedTuple):
    thirsty: bool = False
    sick: bool = False
    sleepy: bool = False


class Rudi:
    def __init__(
        self,
        mouth: typing.Optional[SmoothSensor] = None,
        belly: typing.Optional[SmoothSensor] = None,
        vestibular: typing.Optional[SmoothSensor] = None,
        voice: typing.Optional[BabyCry] = None,
        blatter: typing.Optional[Reservoir] = None,
        joker: typing.Optional[Joker] = None,
        light: typing.Optional[LEDStrip] = None,
    ):
        # sensors
        self.mouth = mouth or SmoothSensor(17)
        self.belly = belly or SmoothSensor(27)
        self.vestibular = vestibular or SmoothSensor(22)
        self._init_sensors()

        # outputs
        self.voice = voice or BabyCry()  # to cry for attention
        self.blatter = blatter or Reservoir(green=23, white=24, yellow=25)  # to pee

        # displays
        self.joker = joker or Joker()
        self.light = light or LEDStrip()

        # internals
        self._condition = Condition()
        self.last_pee: float = 0
        self.next_change: float = time.time() + 60

    def _init_sensors(self) -> None:
        self.mouth.when_activated = lambda: self.reset_condition("thirsty")
        self.belly.when_activated = lambda: self.reset_condition("sick")
        self.vestibular.when_activated = lambda: self.reset_condition("sleepy")

    @property
    def condition(self) -> Condition:
        return self._condition

    def change_condition(self, new_condition: str):
        # Rudi can only ever be in one condition exclusively
        self._condition = Condition(**{new_condition: True})

    def reset_condition(self, condition: str):
        self._condition = self._condition._replace(**{condition: False})

    def cry(self) -> None:
        self.voice.play()

    def pee(self) -> None:
        current_time = time.time()
        # to simulate a slowly filling blatter, Rudi can't pee without a break
        if (current_time - self.last_pee) > 240:
            # update the last pee time
            self.last_pee = current_time

            # release the liquid that indicates the current condition
            duration = 3  # seconds
            if self.condition.sick:
                self.blatter.release_liquid("green", duration)
            elif self.condition.thirsty:
                self.blatter.release_liquid("yellow", duration)
            elif self.condition.sleepy:
                self.blatter.release_liquid("white", duration)

    def update(self) -> None:
        # update joker
        self.joker.update()

        # check Rudi's current condition
        if any(self.condition):
            # something is wrong with Rudi
            self.pee()  # pee, to indicate what's wrong
            self.cry()  # cry, to grab some attention
            # TODO: fire up some light effects with the LED strip for even more
            #       attention
            return

        # everything seems to be ok for now, but maybe not for long
        current_time = time.time()
        if self.next_change <= current_time:
            # a condition change is due, so let's randomly pick a new condition
            self.change_condition(random.choice(Condition._fields))
            # and schedule the next change
            self.next_change = current_time + random.randint(900, 1200)

        # otherwise, we do nothing
